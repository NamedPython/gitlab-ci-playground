FROM registry.gitlab.com/namedpython/rails-heavy-gems:2.5.7-alpine

WORKDIR /usr/src/app

COPY Gemfile .
COPY Gemfile.lock .

RUN bundle install
